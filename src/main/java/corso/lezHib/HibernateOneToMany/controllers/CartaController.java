package corso.lezHib.HibernateOneToMany.controllers;

import corso.lezHib.HibernateOneToMany.models.Carta;
import corso.lezHib.HibernateOneToMany.models.Persona;
import corso.lezHib.HibernateOneToMany.models.crud.CartaDAO;
import corso.lezHib.HibernateOneToMany.models.crud.PersonaDAO;

public class CartaController {
	
	private PersonaDAO perDao;
	private CartaDAO carDao;
	
	public CartaController() {
		this.perDao = new PersonaDAO();
		this.carDao = new CartaDAO();
	}

	public void inserisciCarta(Carta objCar, Persona objPer) {
		
		this.perDao = new PersonaDAO();
		this.carDao = new CartaDAO();
				
		objCar.setProprietario(objPer);
		this.carDao.insert(objCar);
		
	}
	
}
