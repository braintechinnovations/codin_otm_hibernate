package corso.lezHib.HibernateOneToMany;

import corso.lezHib.HibernateOneToMany.models.Carta;
import corso.lezHib.HibernateOneToMany.models.Persona;
import corso.lezHib.HibernateOneToMany.models.crud.CartaDAO;
import corso.lezHib.HibernateOneToMany.models.crud.PersonaDAO;

public class App 
{
    public static void main( String[] args )
    {

    	PersonaDAO perDao = new PersonaDAO();
    	CartaDAO carDao = new CartaDAO();

    	//--------------- INSERIMENTO PERSONA E CARTE CON RELAZIONI ------------
    	
//    	Persona art = new Persona();
//    	art.setPer_nome("Artabano");
//    	art.setPer_cogn("Pastrocchi");
//    	art.setPer_codf("ARTPST");
//    	perDao.insert(art);
//    	
//    	if(art.getId() > 0) {
//    		System.out.println("Persona salvata con successo");
//    		
//    		Carta conadUno = new Carta();
//	    	conadUno.setNegozio("Conad");
//	    	conadUno.setNumero("CND12345");
//	    	conadUno.setProprietario(art);
//	    	
//	    	carDao.insert(conadUno);
//	    	if(conadUno.getId() > 0) {
//	    		System.out.println("Carta salvata");
//	    	}
//	    	
//	    	Carta coopDue = new Carta();
//	    	coopDue.setNegozio("COOP");
//	    	coopDue.setNumero("COP12345");
//	    	coopDue.setProprietario(art);
//
//	    	carDao.insert(coopDue);
//	    	if(coopDue.getId() > 0) {
//	    		System.out.println("Carta salvata");
//	    	}
//    	}
    	
//    	art.getElencoCarte().add(conadUno);
//    	art.getElencoCarte().add(coopDue);
    	
    	//--------------- RECUPER PERSONA E CARTE CON RELAZIONI ------------
    	
    	System.out.println(carDao.getById(7));
    }
}
