package corso.lezHib.HibernateOneToMany.models.crud;

import org.hibernate.Session;

import corso.lezHib.HibernateOneToMany.models.Carta;
import corso.lezHib.HibernateOneToMany.models.Persona;
import corso.lezHib.HibernateOneToMany.models.db.GestoreSessioni;

public class CartaDAO implements Dao<Carta> {

	@Override
	public void insert(Carta t) {

		Session sessione = GestoreSessioni.getInstance().getFactory().getCurrentSession();
		
		try {
			
			sessione.beginTransaction();
			sessione.save(t);
			sessione.getTransaction().commit();
			
		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			sessione.close();
//			System.out.println("Programma terminato");
		}
		
	}

	@Override
	public Carta getById(int id) {
		
		Session sessione = GestoreSessioni.getInstance().getFactory().getCurrentSession();
		
		Carta temp = null;
		
		try {
			
			sessione.beginTransaction();
			temp = sessione.get(Carta.class, id);
			sessione.getTransaction().commit();
			
		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			sessione.close();
//			System.out.println("Programma terminato");
		}
		
		return temp;
		
	}

}
