package corso.lezHib.HibernateOneToMany.models.crud;

import javax.persistence.Entity;

import org.hibernate.Session;

import corso.lezHib.HibernateOneToMany.models.Persona;
import corso.lezHib.HibernateOneToMany.models.db.GestoreSessioni;

public class PersonaDAO implements Dao<Persona>{

	@Override
	public void insert(Persona t) {

		Session sessione = GestoreSessioni.getInstance().getFactory().getCurrentSession();
		
		try {
			
			sessione.beginTransaction();
			sessione.save(t);
			sessione.getTransaction().commit();
			
		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			sessione.close();
//			System.out.println("Programma terminato");
		}
		
	}

	@Override
	public Persona getById(int id) {
		
		Session sessione = GestoreSessioni.getInstance().getFactory().getCurrentSession();
		
		Persona temp = null;
		
		try {
			
			sessione.beginTransaction();
			temp = sessione.get(Persona.class, id);
			sessione.getTransaction().commit();
			
		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			sessione.close();
//			System.out.println("Programma terminato");
		}
		
		return temp;
		
	}

	
	
}
