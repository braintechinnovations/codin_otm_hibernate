package corso.lezHib.HibernateOneToMany.models.crud;

public interface Dao<T> {
	
	void insert(T t);
	
	T getById(int id);
	
}
