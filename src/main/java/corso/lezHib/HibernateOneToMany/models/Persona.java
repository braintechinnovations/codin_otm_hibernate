package corso.lezHib.HibernateOneToMany.models;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="persona")
public class Persona {
	
	@Id
	@GeneratedValue( strategy = GenerationType.IDENTITY )		//Personalizzo la tipologia di generazione della chiave!
	@Column(name="personaID")
	private int id;
	
	@Column(name="nome")
	private String per_nome;
	
	@Column(name="cognome")
	private String per_cogn;
	
	@Column(name="codice_fiscale")
	private String per_codf;
	
	@OneToMany(mappedBy = "proprietario")
	private List<Carta> elencoCarte = new ArrayList<Carta>();
	
	public Persona() {
		
	}

	public Persona(String per_nome, String per_cogn) {
		super();
		this.per_nome = per_nome;
		this.per_cogn = per_cogn;
	}

	public Persona(int id, String per_nome, String per_cogn) {
		super();
		this.id = id;
		this.per_nome = per_nome;
		this.per_cogn = per_cogn;
	}

	
	
	public List<Carta> getElencoCarte() {
		return elencoCarte;
	}

	public void setElencoCarte(List<Carta> elencoCarte) {
		this.elencoCarte = elencoCarte;
	}

	public String getPer_codf() {
		return per_codf;
	}

	public void setPer_codf(String per_codf) {
		this.per_codf = per_codf;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getPer_nome() {
		return per_nome;
	}

	public void setPer_nome(String per_nome) {
		this.per_nome = per_nome;
	}

	public String getPer_cogn() {
		return per_cogn;
	}

	public void setPer_cogn(String per_cogn) {
		this.per_cogn = per_cogn;
	}

	@Override
	public String toString() {
		return "Persona [id=" + id + ", per_nome=" + per_nome + ", per_cogn=" + per_cogn + ", per_codf=" + per_codf + "]";
	}
	
	
}